import { combineReducers } from 'redux'
import actionTypes from "../actions/constants"
import { connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'

const initialAuthState = {
    errors: [],
    user: {
        name: ''
    },
    loading: false
}

const auth = (state = initialAuthState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_REQUEST_SENT:
            return {
                ...state,
                loading: true,
                errors: []
            }
        case actionTypes.AUTH_REQUEST_SUCCESSFUL:
            return {
                ...state,
                token: action.token,
                loading: false
            }

        case actionTypes.AUTH_REQUEST_FAILURE:
            return {
                ...state,
                errors: [...state.errors, action.error],
                loading: false
            }
        case actionTypes.SET_USER_INFO:
            return {
                ...state,
                user: action.user
            }
        case actionTypes.UNSET_USER_INFO:
            return {
                ...state,
                user: {
                    name: ''
                },
            }
        case actionTypes.UNSET_ERRORS:
            return {
                ...state,
                errors: []
            }
        default:
            return state
    }
}

let navigationHistory = createBrowserHistory()

export const rootReducer = (history) => combineReducers({
    auth,
    router: connectRouter(history),
})

export const history = navigationHistory