import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../styles/formErrors.css'

class FormErrors extends Component {
	render() {
		return (
			<div className='errors'>
				{this.props.errors.map((item, i) => {
					return (
						<div className="error" key={i}>
							<span>{ item.message }</span>
						</div>
					)
				})}
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		errors: state.auth.errors
	}
}

export default connect(mapStateToProps, null)(FormErrors)