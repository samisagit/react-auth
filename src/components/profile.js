import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../styles/profile.css'
import { logOut } from '../actions/auth';

class Profile extends Component {
	componentWillMount() {
		this.checkAuth();
	}

	checkAuth() {
		if (!this.props.user.id) {
			this.props.kick()
		}
	}

	render() {
		return (
			<div className='profile'>
            	<h1>Hi {this.props.user.name}</h1>
				<p>This is some placholder content to greet the user.</p>
				<button onClick={(e) => this.props.kick(e)}>Log out</button>
			</div>
        )
	}
}

const mapStateToProps = state => {
	return {
		user: state.auth.user
	}
}

const mapDispatchToProps = dispatch => {
    return {
        kick: () => {
            dispatch(logOut())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)