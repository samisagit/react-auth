import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchAuth, unSetErrors } from '../actions/auth'
import '../styles/signInForm.css';

import FormErrors from './formErrors'

class SignInForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            "username": "jeff1967",
            "password": "hotdog",
        }
    }

    componentWillMount() {
		this.props.loaded();
	}

    handleChange = (event) => {
        let updatedElement = event.target.name
        this.setState({ [updatedElement]: event.target.value })
    }

    render() {
        return (
            <div className="signInForm">
                <form>
                    <legend>Log in</legend>
                    <label><span>username</span>
                    <input
                            name="username"
                            type="text"
                            value={this.state.username}
                            onChange={this.handleChange}
                        />
                    </label>

                    <label><span>password</span>
                    <input
                            name="password"
                            type="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                        />
                    </label>

                    <button onClick={(e) => this.props.onSubmit(e, {
                        "username": this.state.username,
                        "password": this.state.password
                    })}>Log In</button>
                </form>
                {this.props.errors.length > 0 && <FormErrors />}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (e, creds) => {
            e.preventDefault()
            dispatch(fetchAuth(creds.username, creds.password))
        },
        loaded: () => {
            dispatch(unSetErrors())
        }
    }
}

const mapStateToProps = (state) => ({
    errors: state.auth.errors
})

export default connect(mapStateToProps, mapDispatchToProps)(SignInForm)

