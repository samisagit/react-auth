import React, { Component } from 'react';
import './styles/app.css';
import SignInForm from "./components/signInForm"
import Profile from './components/profile'
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router'
import { history } from './reducers'

class App extends Component {
	render() {
		return (
			<div className='app'>
				<ConnectedRouter history={history}>
					<Switch>
						<Route exact path="/" component={SignInForm} />
						<Route exact path="/user" component={Profile} />
					</Switch>
				</ConnectedRouter>
			</div>
		);
	}
}

export default App