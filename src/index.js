import React from 'react'
import ReactDOM from 'react-dom'
import { routerMiddleware } from 'connected-react-router'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { PersistGate } from 'redux-persist/integration/react'
import thunkMiddleware from 'redux-thunk'

import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { rootReducer, history } from './reducers/index'
import config from './config'

const composeMiddleWare = () => {
    if (config.development) {
        return compose(
            applyMiddleware(
                thunkMiddleware,
                routerMiddleware(history),
            ),
        )
    }

    return applyMiddleware(
        thunkMiddleware,
        routerMiddleware(history),
    )
}

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer(history))

const store = createStore(
    persistedReducer,
    composeMiddleWare()
)

const persistor = persistStore(store)

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
