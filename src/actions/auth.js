import actions from "./constants"
import config from "../config"
import { push } from 'connected-react-router'

export const sentAuth = () => ({
    type: actions.AUTH_REQUEST_SENT
})

export const authSuccess = (token) => ({
    type: actions.AUTH_REQUEST_SUCCESSFUL,
    token
})

export const authFail = (error) => ({
    type: actions.AUTH_REQUEST_FAILURE,
    error
})

export const setUserInfo = (user) => ({
    type: actions.SET_USER_INFO,
    user
})

export const unSetUserInfo = () => ({
    type: actions.UNSET_USER_INFO
})

export const unSetErrors = () => ({
    type: actions.UNSET_ERRORS
})

export const logOut = () => (dispatch) => {
    dispatch(unSetUserInfo())
    dispatch(push('/'))
}

export const fetchAuth = (username, password) => (dispatch) => {
    dispatch(sentAuth())

    return fetch(
        config.authEndpoint,
        {
            "method": "POST",
            "headers": {
                "Content-Type": "application/json",
            },
            "body": JSON.stringify({
                "username": username,
                "password": password
            })
        }
    )
    .then(res => {
        if (!res.ok) {
            throw res.text()
        }
        return res.json()
    })
    .then(json => {
            const JWTSplit = json.token.split("."),
            payload = JWTSplit[1],
            decodedPayload = atob(payload)
            dispatch(setUserInfo(JSON.parse(decodedPayload).user))
            dispatch(authSuccess(json.token))
            dispatch(push('/user'))
            
        }
    )
    .catch(async e => {
        const error = await e
        if (typeof error !== 'string') {
            dispatch(authFail(error))
            return
        }
        dispatch(authFail(new Error(error)))
    })
}
